#Enable DNS proxy in NAT mode
VBoxManage modifyvm "VM name" --natdnsproxy1 on
#Use host's resolver as a DNS proxy in NAT mode
VBoxManage modifyvm "VM name" --natdnshostresolver1 on