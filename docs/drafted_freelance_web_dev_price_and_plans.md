The following document help both customer and web designer/developer contractor negotiate.
Note: Prices will vary between project based on the customer/contractors, but factor into current full-time employment rate.

reddit.com/r/web_design is liberal website with freelance people who are veterans/beginners building for themselves/others. 

Example Pricing:
Simple non-changing website update ($500, on the cheaper side)
https://www.reddit.com/r/web_design/comments/cerd4/did_i_charge_a_fair_price/

Average...?
https://www.reddit.com/r/web_design/comments/30tlfc/first_freelance_client_help_working_out_a_price/

https://www.reddit.com/r/web_design/comments/8hqfad/ideal_price_range_for_website_creation/
Expenses(heaviest at front): Full-stack, forum, API usage (customer-identified), forum implementation, mobile capability.

https://www.reddit.com/r/web_design/comments/kn87i/can_i_get_a_sanity_check_here_gave_a_price_and/
Same, heavy-duty, but for relatives.

https://www.reddit.com/r/web_design/comments/5sbomk/how_much_do_you_guys_charge_your_clients_to_build/dddy94n/
Designer getting stiffed
SOLID PRICING BREAKDOWN FOR DEVELOPER WITH LIST OF TASKS:
    https://www.reddit.com/r/web_design/comments/2vs87a/any_thoughts_on_my_prices_wordpress_websites/cokwbek/
SOLID DEADLINE HOURS FOR DEVELOPER
    https://www.reddit.com/r/web_design/comments/8hqfad/ideal_price_range_for_website_creation/dyoa1un/

Getting backup of old project? Migrating project? Handing off project? Transfering files?
https://www.reddit.com/r/web_design/comments/784b7p/700_a_fair_price_to_give_me_a_backup_of_my_own/
Request zipfile with full code and a database dump. Should take 10 minutes.
If the developer decides to charge, if the project is 18 pages of website, it's not worth paying anything, maybe if it's $10-20. DON'T PAY IF THE SITE IS WORDPRESS-BASED. https://www.reddit.com/r/web_design/comments/784b7p/700_a_fair_price_to_give_me_a_backup_of_my_own/dos15m5/

Expected DEADLINE:
    https://www.reddit.com/r/web_design/comments/6mgowg/for_those_of_you_who_plandesignand_develop/
Timelines are soft:
    https://www.reddit.com/r/web_design/comments/7i2b25/how_long_does_it_take_you_to_mock_up_a_pretty/dqvzbdj/
Process:


Price Negotiations:
    As mentioned in the beginning, price is the most difficult part. As a customer, being non-technical can be a problem in knowing you get what you pay for. As a web developer, deciding the time and amount of work is unsure.
    Before customer gets price shock, understand
    So I decide price like this: how much is the customer willing to pay/able to profit off? How much do I need to make this worth?

    Building a strong draft of the project to hand off to you means good foundations, meaning where most of the cost will be. That includes identifying tools the website will use, and what the website will look like. How much the cost is allocated into each depends on the quality of work.

My Price Negotiations:
    Depending on work relations, a percentage will be paid up front, and the rest at the end of the project. Developer's may add more installments of payment depending on trust.
    Without my LLC set up, I pay 30% taxes on whatever I charge. I file a 1099 online received from each of my clients, before January 31. I will request it, but if it is missing, I won't ask for the form.
    https://medium.freecodecamp.org/managing-your-taxes-as-a-freelance-developer-or-startup-3c7dd3d55ffe
    How someone else handled tax-accounted prices:
    https://www.reddit.com/r/web_design/comments/2suysp/how_do_you_guys_decide_on_exact_prices/cnt5vj4/    

Understanding Developer's point of view:
    The developer should pay for the project, not how long the developer spent. A deliverable is a deliverable, and if the developer procrastinates, the customer shouldn't be overcharged.


My Vanilla SLA:
I want my work to be portfolio-able. I don't want to deliver bad quality for customer's end-users. I may not know my timeline/performance speed, but I'll make up for it in responsibility.

Access to your own website pages, database account, domain name (which will be registered under customer name)
Website host security vetting.
Verifying the page works on cross-platforms. (Some people browse without JavaScript for sense of security).
Handoff: If handing off the project to another on developer's account, responsibility in giving the old project with change-tracking through Git, and data dump is minimum. If the handoff is abrupt on contractor's side, 

Add-ons:
  Security advice (DoD security clearance background) (necessary if you log in more than once).
  Hand-off: If the customer decides a change in developer, I can offer paid service to see the skills in a current-solution-is-cheaper.

Certain Price Factors
WordPress? Debatable because of customer's changes conflict with developers
Static->Dynamic Web-sites. +$
Teaching: Teach how to order web space/domain? +$
Training on use? +$$ (ensure by testing common necessities)
Analytics on time? Customers? +$$ to +$$$
Leniency on site reliability? -$
Technological background? -$$
Deadline leniency? -$
Referral program? If you give me leads, and I close those leads, you get 10% of the total project -$$$
Isolated, not shared space? +$

Separate Service Fees:
Site migration (timely manner). ($)
Domain fixes. ($)

Mentions:
Relationship with web design/developer contractors are important to both sides:
    - Important to your web contractor/designer because of word of mouth.
    - Important to you, the customer, to keep good services and price. Being understanding of the price.
        - That being said, getting price quotes before 
    

My checklist:
    Who holds liability of website?
    Security checklist for web developer:
        https://news.ycombinator.com/item?id=12140477
        https://blog.logrocket.com/security-for-fullstack-web-developers-part-1-a56340283f7c
    https://webplatform.github.io/ - Open Source information
    Squarespace - for photographers. 
        API usage (identified)
    Review Schedule C (I can't be an employee)
    ReferralProgram
    Finding clients:
        https://www.reddit.com/r/web_design/comments/5ddmzm/what_are_some_good_tips_for_landing_new_clients/da4j87k/
    "Mock Up"
        Don't ask for feedback, present the "mock-up"
        https://www.reddit.com/r/freelance/comments/91pxjp/question_for_you_front_end_devs/e3042l7/
    Rough estimate
        http://thenuschool.com/how-much/#/done
    Buzzword Docker/Kubernetes?
        https://news.ycombinator.com/item?id=15556691
            Containers achieve security benefits and resolve dependency/differences.
    Designs/I can learn from them:
        https://www.reddit.com/r/web_design/comments/2suysp/how_do_you_guys_decide_on_exact_prices/cnt5vj4/
    Quick prototyping:
        https://medium.freecodecamp.org/how-to-prototype-websites-quickly-with-css-grid-ffc9cba08583
    PRETTY HONEST PRICING:
        https://www.reddit.com/r/web_design/comments/5sbomk/how_much_do_you_guys_charge_your_clients_to_build/ddepdm5/
    Don't share git version changes nor explanations in Git.
