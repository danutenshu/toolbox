#!/bin/bash

# Get OS name
if [ "$(uname)" == "Darwin" ]; then
    OS="MacOSX"
    # Do something under Mac OS X platform
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    OS="Linux"
    if [ -f /etc/redhat-release ] ; then
        DIST='redhat'
        PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
        REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
    elif [ -f /etc/SuSE-release ] ; then
        DIST=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
        REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
    elif [ -f /etc/mandrake-release ] ; then
        DIST='mandrake'
        PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
        REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
    elif [ -f /etc/debian_version ] ; then
        DIST="debian"
        REV="cat /etc/debian_version"

    fi
    if [ -f /etc/UnitedLinux-release ] ; then
        DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
    fi
    # Do something under GNU/Linux platform
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    OS="win32"
    # Do something under 32 bits Windows NT platform
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    OS="win64"
    # Do something under 64 bits Windows NT platform
fi





if [ "$OS" == "Linux" ] ; then
    echo $DIST;
else
    echo $OS;
fi



if [ ! -f toolboxconfig ]; then
    >&2 echo "$0: File 'toolboxconfig' in current directory not found."
fi
source toolboxconfig

if [[ "$PWD" != /tmp/toolbox ]]; then
    >&2 echo "$0: Script configuration error. Download repo to /tmp/toolbox"
fi

echo "$0: Running os script for OS '$OS'"
sh $TOOLBOX_DIR/bin/./os-$OS.sh
echo "$0: Running sequence to configure git"
sh $TOOLBOX_DIR/bin/./sequence_git_dev.sh
echo "$0: Running sequence to install rails from source"
sh $TOOLBOX_DIR/bin/./sequence_rails.sh
echo "$0: Machine setup sequence complete"