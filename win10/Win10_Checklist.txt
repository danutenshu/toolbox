1 Hours - Check through old settings
2 Hours - Set configurations

============================
Win10 Settings Configuration
============================
Disable Cortana, taskbar views "Taskview" and "People" button
Disable Location
Disable Feedback data to Microsoft
Edit color scheme
Review background apps
Night Light - 9AM-9PM
Lock Computer after 5 minutes
Sticky keys
Big cursor (Windows Default (large)(system scheme))
Rename PC
Background
Configure printers

============================
COTS
============================
Google Chrome
	> Work PC: danielcareermail@gmail.com
	> Home PC: danutenshu@gmail.com
	> Memorandum Email: danutenshu@gmail.com
	> LastPass: DSHU-<Corporation><PC_Label>
	Microsoft Visual Studio	
	> Shellcheck by Timon Wong	
	> Markdown lint by David Anson
KeePass 2
	> Import Settings
Notepad2
	> View -> Default font -> ProFontWindows
Java SDK
  > Environment Variables: JAVA_HOME
PuTTY(thru .msi)\PuTTY CAC - Two options...
	1) Import settings from regedit
		https://stackoverflow.com/questions/13023920/how-to-export-putty-sessions-list
	2) Manually set up
	> Terminal -> Bell -> 'Set the style of Bell': Visual bell
	> Windows -> Appearance -> 'Font settings': ProFontWindows, 10-point | 'Font quality': Non-Antialiased (ProFontWindows works better non-antialiased)
	> Attempt certificate authentication
	> Bastion->Category: SSH->Auth->Authentication Parameters->Check 'Allow Agent Forwarding'
RDP (add to start menu)
ConEmu Alpha
	> Import Settings
	> Windows ProFont
	> Install tmux
	> Install UnxUtils
WinSCP
Wireshark
Microsoft's Sysinternals Suite (unzip into User folder and add to environment variables)
	Tcpview
7-Zip
Git Bash
	> git config --List
	> Import ~/.gitconfig
		[user]
			name = Danel Shu
			email = Daniel.Shu@VSolvit.com
	> Install tmux
Python
grepWin
Sublime
AWS CLI
IntelliJ

Acrobat Reader DC
WSL for Ubuntu
	> mkdir .ssh
	> vim .ssh/config
Host machine
   HostName 192.168.12.34
   User dshu
   port 22
	> vim .gitconfig
[user]
       name = Daniel Shu
       email = danutenshu@gmail.com
[core]
       editor = vim
[push]
       default = simple
[diff]
       tool = vimdiff
	> vim .vimrc
		set visualbell
PostgreSQL


SourceTree
VMware Workstation
VMware vSphere
  > Verify Input settings
VirtualBox
  > Add `C:\Program Files\Oracle\VirtualBox` to env variable `PATH`
Apache Tomcat
ActivIdentity(?)
Sysinternals Suite's sysmon
nmap

Vagrant
Chef
Microsoft Office -> Pin to Taskbar -> Word 2016 Excel 2016
  > KuTools for Microsoft Excel Formula Helper
Spotify
VLC


IDONTKNOW
=========
ActivClient
InstallRoot
Windows 10 Update Assistant



===========
Exporting
============
> Check Windows Start Menu
> Check Environment Variables
	JAVA_HOME (jdk)
	M3_HOME (C:\Apache\maven\apache-maven-3.5.2)
	MAVEN_HOME (Same above)
	mvn (...\bin)
	PATH 	(
		+
			
		Verify
			%JAVA_HOME%\bin
			%M2_HOME%\bin
			C:\PROGRA~1\Python36\Scripts\
			C:\HashiCorp\Vagrant\bin
		)
> Check C: drive