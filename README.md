# toolbox - Daniel's Personal Environment-Building Library
Use the shell scripts from this repo to automate software environments for Daniel Shu. Basic automation scripts to configure environments without extra CM tools like Chef or Ansible.


## Usage
**Machine deployment**: Scripts are geared for Linux/Unix-like machines.

Clone this repo to the machine's /tmp/ folder



*Make corrections or report issues with plug and play.*


## Additional Information
[Digital Ocean's take on initial server setup](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-centos-7)

[Per Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-on-centos-7), disable root ssh login

[How to configure nginx https and redirect from http requests](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-on-centos-7)

[rbenv+rails](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-centos-7)

### VM Configuration
**For VM hosted on VirtualBox**
From [stackexchange](https://unix.stackexchange.com/questions/145997/trying-to-ssh-to-local-vm-ubuntu-with-putty), configure port forwarding for 127.0.1.1:XXXX to 10.0.2.X:3000 (as well as for ssh). Firewall must be enabled for long term, though should be disabled when not available.
