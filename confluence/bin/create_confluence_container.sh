# TODO: Make container creation unique by either
# 1. Adding hash string generator and add hexstring to make name unique.
# 2. Add the container IDs hash to name afterwards.

docker run -v /data/your-confluence-home:/var/atlassian/application-data/confluence --name="confluence" -d -p 8090:8090 -p 8091:8091 atlassian/confluence-server
