/**
 * Author: Daniel Shu
 * Description: Creates PostgreSQL roles and databases for a Confluence server.
 * References:
 *  https://www.postgresql.org/docs/12/role-membership.html
 **/
CREATE ROLE confluence_dml;
CREATE ROLE confluence LOGIN INHERIT;
GRANT confluence_dml TO confluence;
CREATE DATABASE prod_confluence WITH OWNER confluence ENCODING utf8;
GRANT CREATE ON DATABASE prod_confluence TO confluence_dml;