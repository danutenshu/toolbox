/**
 * Author: Daniel Shu
 * Description: Creates PostgreSQL roles and databases for a Confluence server.
 * References:
 *  https://www.postgresql.org/docs/12/role-membership.html
 **/
CREATE ROLE dushu LOGIN INHERIT;
GRANT confluence_dml TO dushu;
