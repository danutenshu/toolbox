# Install Confluence
These are instructions to install Confluence on a container with a PostgreSQL VM.

## Instruction Sequence

1. `install_postgresql_centos8.sh` - Install PostgreSQL VM on a Linux machine
2. Edit PostgreSQL's pg_hba.conf and postgresql.conf
2. `create_postgresql_confluence.sql` - Create roles and databases.

## Extra Steps
Once the machine is configured, navigate to Marketplace apps to install the following add-on(s).

* "Source Editor" - Edit the page's raw code if the spacing looks off.