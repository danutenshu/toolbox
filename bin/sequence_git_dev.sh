#!/bin/bash

git config --global color.ui true
git config --global user.name "Daniel Shu"
git config --global user.email "danutenshu@gmail.com"
git config --global core.editor vim