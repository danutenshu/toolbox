#!/bin/bash

RUBY_VERSION = "2.6.5"
GIT_NAME = "Daniel Shu"
GIT_EMAIL = "dautenshu@gmail.com"
if [ -z "$RUBY_VERSION" ] | [ -z "$GIT_NAME"] | [ -z "$GIT_EMAIL"];
then
      >&2 echo "$0: Environment variable RUBY_VERSION not found."
fi

apt-get update -y

# One machine for one-person projects
echo "Install developer tools..."
apt-get install sudo git tree tmux vim curl


cat > ~/.bash_aliases <<EOF
alias go='cd ~/Documents/ruby-thoughtmodel'
alias ll='ls -alF'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
EOF

apt-get install postgresql postgresql-contrib libpq-dev

git clone https://gitlab.com/danutenshu/ruby-thoughtmodel.git ~/Documents