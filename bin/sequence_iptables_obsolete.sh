#!/bin/sh
# My system IP/set ip address of server
SERVER_IP="45.33.47.154"
#SERVER_IP=" . . . "

# Flushing all rules
iptables -F
iptables -X
# Setting default filter policy
###############################
# Debugging purposes
###############################
#iptables -P INPUT REJECT
#iptables -P OUTPUT REJECT
#iptables -P FORWARD REJECT
###############################
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
# Allow unlimited traffic on loopback
iptables -A INPUT -i lo -j ACCEPT
# iptables -A OUTPUT -o lo -j ACCEPT
 
 
 #Website Protocols
iptables -A INPUT -s 192.168.122.1/32 -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A INPUT -s 192.168.123.0/24 -j DROP
iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 21 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 20 -j ACCEPT
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
iptables -A OUTPUT -o eth0 -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth0 -p tcp --sport 443 -m state --state ESTABLISHED -j ACCEPT