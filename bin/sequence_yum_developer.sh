#!/bin/bash

# Installs personal development tools using yum

yum install -y vim tree git

cat <<EOF >~/.vimrc
colorscheme delek
EOF