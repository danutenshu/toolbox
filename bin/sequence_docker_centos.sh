#!/bin/bash
# https://docs.docker.com/engine/install/centos/

# Configure yum-utils package for Docker
yum install -y yum-utils
yum-config-manager --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker Engine
yum install docker-ce docker-ce-cli containerd.io
# GPG key: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35

systemctl enable docker
systemctl start docker