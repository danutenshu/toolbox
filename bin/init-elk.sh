VERSION=0.20.6

sudo apt-get update
sudo apt-get install openjdk-6-jdk

wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-$VERSION.deb
sudo dpkg -i elasticsearch-$VERSION.deb

# be sure you add "action.disable_delete_all_indices" : true to the config!!

# start script
sudo /etc/init.d/elasticsearch restart

# ------------------------------------------------------------------------------------

# if you want to remove it:
#sudo dpkg -r elasticsearch

# binaries & plugin
#/usr/share/elasticsearch/bin

# log dir
#/var/log/elasticsearch

# data dir
#/var/lib/elasticsearch

# config dir
#/etc/elasticsearch

# prepare ElasticSearch UI
#sudo apt-get install apache2 
#sudo mkdir /var/www/ui
#sudo chown -R ubuntu.www-data /var/www
# now copy ES-HEAD to /var/www/ui