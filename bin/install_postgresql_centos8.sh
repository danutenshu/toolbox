#!/bin/bash

# Author: Daniel Shu

# Installs PostgreSQL 12 on CentOS 8.
# After this script is installed, user should redefine the listen_address:
#   /var/lib/pgsql/12/data/postgresql.conf
#
#
# Improvements can be made to make script OS-agnostic
#
# References:
# 
# https://confluence.atlassian.com/doc/database-setup-for-postgresql-173244522.html
#

# Hostname - San Diego, Data (Unraid)/Test (RPi K8S), 
hostnamectl set-hostname SDDV-PSQL02
# Update definitions
yum update -y
yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# PostgreSQL
dnf -qy module disable postgresql
yum install postgresql12-server postgresql12 -y
# TODO: Harden...
# - Account-level access to postgresql binaries
# - 
# Edit pg_hba.conf
# /usr/pgsql-12/bin/pg_ctl reload --pgdata=/var/lib/pgsql/12/data
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable --now postgresql-12
# MANUAL: Define password for postgres
sudo su postgres -c "psql -U postgres -f ../confluence/lib/create_postgresql_confluence.sql"

# TODO: Box the installation and configuration into different functions

# TODO: In services like Confluence, edit pg_hba.conf to allow access for service account.


# TODO: vi /var/lib/pgsql/12/data/postgresql.conf 
#           listen_addresses = '*'
# TODO: vi /var/lib/pgsql/12/data/pg_hba.conf
#        for postgresql and DSHU-AQUAMAN access
#  host    prod_confluence confluenceservice confluence            md5
#  host    postgres        postgres        10.20.0.33              md5
#           table pg_hba_file_rules;

# psql -Uconfluenceservice postgres




#firewall-cmd --add-service=postgresql --permanent
#firewall-cmd --reload
#firewall-cmd --list-all