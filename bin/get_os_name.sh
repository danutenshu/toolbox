#!/usr/bin/env bash
# Detects OS type (GNU/Linux, Mac OS X, Windows NT)
# Code and advice taken from https://stackoverflow.com/a/17072017, author: https://stackoverflow.com/users/1576184/albert

# Good reference pages
# https://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux/18790824
# https://stackoverflow.com/questions/26988262/best-way-to-find-os-name-and-version-in-unix-linux-platform
# https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script

if [ "$(uname)" == "Darwin" ]; then
    OS="MacOSX"
    # Do something under Mac OS X platform
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    OS="Linux"
    if [ -f /etc/redhat-release ] ; then
        DIST='RedHat'
        PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
        REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
    elif [ -f /etc/SuSE-release ] ; then
        DIST=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
        REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
    elif [ -f /etc/mandrake-release ] ; then
        DIST='Mandrake'
        PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
        REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
    elif [ -f /etc/debian_version ] ; then
        DIST="Debian"
        REV="cat /etc/debian_version"

    fi
    if [ -f /etc/UnitedLinux-release ] ; then
        DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
    fi
    # Do something under GNU/Linux platform
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    OS="win32"
    # Do something under 32 bits Windows NT platform
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    OS="win64"
    # Do something under 64 bits Windows NT platform
    
fi

if [ "$OS" == "Linux" ] ; then
    echo $DIST;
else
    echo $OS;
fi
