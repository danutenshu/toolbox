#!/bin/bash
###############################################################################
### Written by Daniel Shu
### A configurable setting template. Replace all settings within "< >" 
###  characters.
###
### Description: changes machine time, sets up user, configures iptables, 
### 
### 
### Installed Tools:
### dos2unix
### tmux
### nginx
### sudo systemctl start postgresql-10.service
### su - postgres -c "psql"
###############################################################################
echo "Daniel's CentOS command sequences.\m*Notice* Please practice safe execution by knowing what this script does.\nThere are necessary changes in this script surrounded by characters < >."
read -n 1 -s -r -p " Press any key to continue"

uname -a
rpm --query centos-release
hostnamectl set-hostname <machine_name>

visudo
groupadd admin
sudo adduser <user_1> -g admin
sudo passwd <user_1>
ip addr; sudo ifup enp0s3

sudo curl -O https://rpmfind.net/linux/centos/6.9/os/x86_64/Packages/tree-1.5.3-3.el6.x86_64.rpm
# sudo curl -O https://rpmfind.net/linux/centos/7.4.1708/os/x86_64/Packages/tree-1.6.0-10.el7.x86_64.rpm
sudo yum --nogpgcheck localinstall tree-1.5.3-3.el6.x86_64.rpm -y
# root shell change to /sbin/nologin
sudo yum install epel-release -y
sudo yum install nginx -y]
sudo yum install tmux dos2unix

# Edit terminal shortcut to Win+T and terminal color scheme to GRN/BLK

### Edit nginx website
sudo mkdir /var/www/
sudo mkdir /var/www/<site_name>
sudo vim /etc/nginx/nginx.conf
sudo mkdir /var/www/<site_name>/public_html
sudo systemctl start nginx

###
iptables-save > iptables-rule.bkp
firewall-cmd --permanent --add-port=3000/tcp
firewall-cmd --reload

### Change port 
rpm -Uvh https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-02.noarch.rpm
# sudo curl -O https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-redhat10-10-2.noarch.rpm
# sudo yum --nogpgcheck localinstall pgdg-redhat10-10-2.noarch.rpm -y
sudo yum install postgresql10-server postgresql10 -y
/usr/pgsql-10/bin/postgresql-10-setup initdb
# PostgreSQL data directory Path: /var/lib/pgsql/10/data/
# sudo yum install postgresql-server postgresql-contrib -y; sudo postgresql-setup initdb
systemctl enable postgresql-10.service --now
# systemctl start postgresql-10.service; systemctl enable postgresql-10.service
su - postgres -c "psql"

cd /etc/nginx

### Install Javascript Runtime for Rails
sudo yum -y install epel-release nodejs

### Install Rails
# https://www.pluralsight.com/guides/ruby-ruby-on-rails/how-to-install-ruby-on-rails-on-centos-7
# https://github.com/rbenv/rbenv
# If there is an issue, install rbenv-doctor
$ sudo yum install -y git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel
cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
vim ~/.bash_profile
# Add rbenv to existing $PATH
## <PATH=$PATH:$HOME/.local/bin:$HOME/bin:$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:~/.rbenv/shims:usr/local/bin:/usr/bin:/bin>
## echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
exec $SHELL
# Set up ruby-build
mkdir -p "$(rbenv root)"/plugins
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bash_profile
exec $SHELL
rbenv install 2.6.5 > rb_2.6.5_installation.log
rbenv local 2.6.5
rbenv global 2.6.5
rbenv shell 2.6.5
export RBENV_VERSION=2.6.5
export RBENV_ROOT=~/.rbenv
export RBENV_DIR=$PWD
gem install rails -v 5.2.0
gem install bundler -v 2.1.4
rbenv rehash
# Check if rbenv installed alright
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
### Install NodeJS (JS Runtime for Rails)
yum -y install epel-release
curl -L -o nodesource_setup.sh https://rpm.nodesource.com/setup_14.x
yum install nodejs
### Install Yarn
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg
yum install yarn

su - postgres -c "createuser -s developer"
# \password developer
rails new devapp -d postgresql
cd devapp/
vim config/database.yml
# Enter username developer and password
vim Gemfile
bundle install
rake db:create
# 



sudo vim /etc/passwd
# Disable login for root

# systemctl vs service
# Do I need dos2unix
# Checking a computer's old configuration (/etc/sysconfig/iptables)
