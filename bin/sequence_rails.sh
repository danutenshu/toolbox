#!/bin/bash

# Install Ruby on Rails
## Install rbenv
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
~/.rbenv/bin/rbenv init
LINE='eval "$(rbenv init -)"'
if ! grep -qF "$LINE" ~/.bashrc ; then echo "$LINE" >> ~/.bashrc ; fi
source ~/.bashrc
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash

## Update ruby-build
mkdir -p "$(rbenv root)"/plugins
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
cd ~/.rbenv/plugins/ruby-build; git pull;
rbenv install $RUBY_VERSION
rbenv global $RUBY_VERSION
gem install bundler rails

apt-get install nodejs

## Install PostgreSQL
#wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
#sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
#sudo apt-get update
#apt-get install postgresql postgresql-contrib libpq-dev
gem install pg
apt-get install postgresql postgresql-contrib libpq-dev