#!/bin/bash

# Prints random 32 character password.

tr -dc A-Za-z-0-9 < /dev/urandom | head -c 32; echo