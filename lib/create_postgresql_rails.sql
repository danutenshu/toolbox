/**
 * Author: Daniel Shu
 * Description: Creates PostgreSQL roles and databases for a Rails project ruby-thoughtmodel.
 * References:
 *  https://www.postgresql.org/docs/12/role-membership.html
 **/
CREATE ROLE ruby_thoughtmodel_service WITH LOGIN CREATEDB SUPERUSER PASSWORD 'ruby'; /* SELECT, READ/INSERT/UPDATE */
/**
CREATE DATABASE ruby_thoughtmodel_dev WITH OWNER ruby_thoughtmodel_service ENCODING utf8;
GRANT ALL PRIVILEGES ON DATABASE ruby_thoughtmodel_dev TO ruby_thoughtmodel_service;
**/
