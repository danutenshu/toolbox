/**
 * Author: Daniel Shu
 * Description: Creates PostgreSQL roles and databases for a Confluence server.
 * References:
 *  https://www.postgresql.org/docs/12/role-membership.html
 **/
CREATE ROLE confluence_read; /* SELECT */
CREATE ROLE confluence_dml; /* SELECT, READ/INSERT/UPDATE */
CREATE ROLE confluence_adm; /* SELECT, READ/INSERT/UPDATE, CREATE/DROP */
CREATE ROLE confluenceservice LOGIN INHERIT; 
GRANT confluence TO confluenceservice;
CREATE DATABASE db_confluence WITH OWNER confluenceservice ENCODING utf8;
GRANT CREATE ON DATABASE db_confluence TO confluence_adm;